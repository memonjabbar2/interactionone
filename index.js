const express = require('express');
const bodyParser = require('body-parser');
var app = express();
const request = require('request');
var path = require('path');
const mongoDB = require('./database');
mongoDB.connect();

let Weather = require('./weatherModel');

require('dotenv').config()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}))

app.get("/", (req, res) => {
    res.sendFile(__dirname + '/app.html');
})

app.get("/basic", (req, res) => {
    res.send("Hello World");
})
app.post('/getWeather', async (req, res) => {

    let cityName = req.body.city_name;

    console.log("City Name \n", cityName);

    let reqUrl = process.env.URL + cityName + "&appid=" + process.env.API_KEY
    request(reqUrl, async function (err, response, body) {
        if (err) {
            console.log("Error :\n", err)
            return;
        }
        console.log("Weather Data of City \t", cityName, "\t \t \t", body);
        body = JSON.parse(body);

        if (body.cod === 200) {
            //********* Insert Into DB */
            const weatherItem = new Weather({
                coord: body.coord,
                temperature: body.main,
                city_name: body.name
            })
            await weatherItem.save(function (err, document) {
                if (err) {
                    console.log("Error \n", err);
                }
                console.log("Document Added Successfully \n", document)
                res.send(body);
               
            })
        } else {
            res.send(body);
        }

    })
})

app.listen(process.env.PORT || 3005, () => {
    console.log("started web process at Port 3005");
});