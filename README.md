# Project Title
Weather Forecasting

---
## Requirements

For development, you will only need Node.js and a node global package, NPM, installed in your environement.

## Install

    $ git clone https://gitlab.com/memonjabbar2/interactionone.git
    $ cd PROJECT_TITLE
    $ npm install
## Running the project

    $ node index.js
